<?php
/**
 * @file
 * Forms and function for the objects admin page.
 */

/**
 * GWO objects admin form.
 */
function gwo_objects_admin_form($form, $form_state) {
  try {
    $service = gwo_service();
    $classList = $service->loyaltyclass->listLoyaltyclass(_gwo_issuer_id());
  } catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
    return array();
  }
  $rows = array();
  $count = 0;
  $resources = array();
  foreach ($classList->getResources() as $class) {
    /* @var $class Google_Service_Walletobjects_LoyaltyClass */
    if (isset($_GET['classId']) && $class->getId() !== check_plain($_GET['classId'])) {
      // We are filtering out objects that don't match the classId.
      continue;
    }
    try {
      $result = $service->loyaltyobject->listLoyaltyobject($class->getId());
    } catch (Exception $e) {
      drupal_set_message($e->getMessage(), 'error');
      return array();
    }
    if ($result && $result instanceof Google_Service_Walletobjects_LoyaltyObjectListResponse) {
      $resources = array_merge($resources, $result->getResources());
    }
  }

  foreach ($resources as $obj) {
    /* @var $obj Google_Service_Walletobjects_LoyaltyObject */
    $debug_style = '';
    if ($_GET['objectId'] === $obj->getId()) {
      $debug_style = 'border: 2px solid blue;background-color:yellow;';
    }
    $count++;
    $rows[] = array(
      'style' => $debug_style,
      'data' => array(
        _gwo_parse_id($obj->getId()),
        _gwo_parse_id($obj->getClassId()),
        $obj->getState(),
        $obj->getVersion(),
        _gwo_objects_admin_form_operations($obj),
      ),
    );
  }

  $header_title = t('Displaying all objects (@count)', array('@count' => $count));
  if (isset($_GET['classId'])) {
    $class_id = check_plain($_GET['classId']);
    $class_name = _gwo_parse_id($class_id);
    $header_title = t('Displaying objects for %className (@count)', array(
      '%className' => $class_name,
      '@count' => $count,
    ));
  }
  $form['header'] = array(
    '#type' => 'item',
    '#title' => $header_title,
  );
  $form['table'] = array(
    '#theme' => 'table',
    '#rows' => $rows,
    '#header' => array(
      t('ID'),
      t('Class'),
      t('State'),
      t('Version'),
      t('Operations'),
    ),
  );

  if (isset($_GET['debug']) && $_GET['debug'] === 'true') {
    $form['debug'] = array(
      '#type' => 'fieldset',
      '#title' => t('Debug Information'),
    );
    $object_id = check_plain($_GET['objectId']);
    try {
      $debug_object = reset(array_filter($resources, function ($r) use ($object_id) {
        return $r->getId() == $object_id;
      }));
      $form['debug']['back']['#markup'] = l(t('Hide Debug'), current_path());
      $form['debug']['info']['#markup'] = '<pre>' . var_export($debug_object, TRUE) . '</pre>';
    } catch (Exception $e) {
      drupal_set_message($e->getMessage(), 'error');
    }
  }

  return $form;
}

/**
 * Get operations for an object.
 */
function _gwo_objects_admin_form_operations(Google_Service_Walletobjects_LoyaltyObject $obj) {
  $output = '';
  $output[] = l(t('Debug'), current_path(), array(
    'query' => array(
      'debug' => 'true',
      'objectId' => $obj->getId(),
    ),
  ));
  return implode(' | ', $output);
}
