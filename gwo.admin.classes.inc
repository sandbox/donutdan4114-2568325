<?php
/**
 * @file
 * Forms and functions for classes admin page.
 */

/**
 * GWO classes admin page.
 */
function gwo_classes_admin_form($form, $form_state) {
  try {
    $service = gwo_service();
    $result = $service->loyaltyclass->listLoyaltyclass(variable_get('gwo_issuer_id'));
  } catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
    return array();
  }
  $rows = array();
  global $user;
  if ($result && $result instanceof Google_Service_Walletobjects_LoyaltyClassListResponse) {
    $resources = $result->getResources();
    foreach ($resources as $class) {
      /* @var $class Google_Service_Walletobjects_LoyaltyClass */
      $object_id = 'test_' . _gwo_parse_id($class->getId()) . '_' . variable_get('site_name') . '_' . $user->uid;
      $object = gwo_create_loyalty_object($object_id, _gwo_parse_id($class->getId()));
      $jwt = gwo_jwt(array('loyaltyObjects' => array($object)));
      $save_to_wallet_button = array(
        '#type' => 'save_to_wallet',
        '#jwt' => $jwt,
      );

      $debug_style = '';
      if ($_GET['classId'] === $class->getId()) {
        $debug_style = 'border: 2px solid blue;background-color:yellow;';
      }
      $rows[] = array(
        'style' => $debug_style,
        'data' => array(
          str_replace(variable_get('gwo_issuer_id') . '.', '', $class->getId()),
          $class->getIssuerName(),
          $class->getProgramName(),
          $class->getKind(),
          render($save_to_wallet_button),
          _gwo_classes_admin_form_operations($class),
        ),
      );
    }
  }
  $form['table'] = array(
    '#theme' => 'table',
    '#rows' => $rows,
    '#header' => array(
      t('ID'),
      t('Issuer Name'),
      t('Program Name'),
      t('Type'),
      t('Button'),
      t('Operations'),
    ),
  );

  if (isset($_GET['debug']) && $_GET['debug'] === 'true') {
    $form['debug'] = array(
      '#type' => 'fieldset',
      '#title' => t('Debug Information'),
    );
    $class_id = check_plain($_GET['classId']);
    try {
      $debug_class = reset(array_filter($resources, function ($r) use ($class_id) {
        return $r->getId() == $class_id;
      }));
      $form['debug']['back']['#markup'] = l(t('Hide Debug'), current_path());
      $form['debug']['info']['#markup'] = '<pre>' . var_export($debug_class, TRUE) . '</pre>';
    } catch (Exception $e) {
      drupal_set_message($e->getMessage(), 'error');
    }
  }
  else {
    // Only show "Create New" form when not debugging.
    $form['new'] = array(
      '#type' => 'fieldset',
      '#title' => t('Create New...'),
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('Create a new default class. You can edit individual options in the Google Wallet interface.'),
    );
    $form['new']['class']['type'] = array(
      '#type' => 'select',
      '#options' => array(
        'loyalty' => t('Loyalty'),
        'offer' => t('Offer'),
        'giftcard' => t('Gift Card'),
      ),
      '#empty_option' => t('-- Select a Type --'),
    );
    $form['new']['class']['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Class Name'),
    );
    $form['new']['class']['button'] = array(
      '#type' => 'button',
      '#value' => t('Create'),
      '#executes_submit_callback' => TRUE,
      '#validate' => array('gwo_classes_admin_form_create_class_validate'),
      '#submit' => array('gwo_classes_admin_form_create_class'),
    );
  }
  return $form;
}

/**
 * Validation function for creating a new class.
 */
function gwo_classes_admin_form_create_class_validate($form, $form_state) {
  $v = $form_state['values'];
  if (empty($v['new']['class']['type'])) {
    form_set_error('new][class][type', t('Please select a class type.'));
    return;
  }
  $name = trim($v['new']['class']['name']);
  if (empty($name)) {
    form_set_error('new][class][name', t('Please enter a class name.'));
    return;
  }
}

/**
 * Submit handler for creating a new class.
 */
function gwo_classes_admin_form_create_class($form, $form_state) {
  $v = $form_state['values'];
  try {
    switch ($v['new']['class']['type']) {
      case 'loyalty':
        $result = gwo_service()->loyaltyclass->insert(gwo_create_loyalty_class($v['new']['class']['name']));
        break;
      case 'offer':
        // @todo: Finish this functionality.
        $result = FALSE;
        break;
      case 'giftcard':
        // @todo: Finish this functionality.
        $result = FALSE;
        break;
    }
  } catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
    return;
  }
  if ($result) {
    // No errors.
    drupal_set_message(t('Created new class: %class', array('%class' => $result->getId())));
  }
  else {
    drupal_set_message(t('An unknown error occurred when trying to create the class.'), 'error');
  }
}

/**
 * Get operations for a class.
 */
function _gwo_classes_admin_form_operations(Google_Service_Walletobjects_LoyaltyClass $class) {
  $output = '';
  $base_id = _gwo_parse_id($class->getId());
  $view_link = _gwo_merchant_link('View', 'classes/' . $base_id . '/');
  $output[] = $view_link;
  $output[] = l(t('Objects'), 'admin/config/services/gwo/objects', array(
    'query' => array(
      'classId' => $class->getId(),
    ),
  ));
  $output[] = l(t('Debug'), current_path(), array(
    'query' => array(
      'debug' => 'true',
      'classId' => $class->getId(),
    ),
  ));
  return implode(' | ', $output);
}