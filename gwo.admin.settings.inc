<?php
/**
 * @file
 * Admin settings page forms and functions.
 */

/**
 * GWO admin settings form.
 */
function gwo_settings_admin_form($form, $form_state) {
  // General config settings.
  $form['config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configuration Settings'),
    '#description' => t('External links: !account | !classes | !objects | !disc', array(
      '!account' => _gwo_merchant_link('Account Management', 'account'),
      '!classes' => _gwo_merchant_link('View Classes', 'classes'),
      '!objects' => _gwo_merchant_link('View Objects', 'objects'),
      '!disc' => _gwo_merchant_link('View Discoverables', 'objects'),
    )),
  );
  $form['config']['gwo_service_account_email_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Service Account Email Address'),
    '#default_value' => variable_get('gwo_service_account_email_address', ''),
    '#description' => t('Google Developers Console > APIs & Auth > Credentials.<br />Email should look similar to "%example".',
      array('%example' => rand(100000, PHP_INT_MAX) . '-' . user_password(32) . '@developer.gserviceaccount.com')),
    '#required' => TRUE,
  );
  $form['config']['gwo_issuer_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Issuer ID'),
    '#default_value' => _gwo_issuer_id(),
    '#description' => t('This is your unique account ID number, used to identify you.<br />Can be found !here under Account Management.', array('!here' => l(t('here'), 'https://wallet.google.com/merchant/walletobjects', array('attributes' => array('target' => '_blank'))))),
    '#required' => TRUE,
  );
  $form['config']['gwo_application_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Application Name'),
    '#default_value' => variable_get('gwo_application_name', ''),
    '#description' => t('Name of your application (used for oAuth, can be anything).'),
    '#required' => TRUE,
  );
  $form['config']['gwo_origins'] = array(
    '#type' => 'textarea',
    '#title' => t('Origins'),
    '#default_value' => variable_get('gwo_origins', ''),
    '#description' => t('Application origins (url paths) for "Save to Wallet" button.<br />Place each URL path on a new line.'),
  );
  $form['config']['gwo_service_account_private_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Private Key Path'),
    '#default_value' => variable_get('gwo_service_account_private_key', ''),
    '#description' => t('Path to the private key file (relative to the Drupal site).'),
    '#required' => TRUE,
  );

  if (_gwo_issuer_id()) {
    // Action buttons.
    $form['actions']['test'] = array(
      '#type' => 'submit',
      '#value' => t('Test Connection'),
      '#ajax' => array(
        'callback' => 'gwo_settings_admin_form_test_connection',
        'wrapper' => 'gwo-test-connection-results',
        'effect' => 'fade',
        'method' => 'append',
        'progress' => array(
          'type' => 'throbber',
          'message' => t('Connecting to Google Wallet API...'),
        ),
      ),
      '#submit' => array('gwo_settings_admin_form_test_connection'),
      '#weight' => 2,
      '#suffix' => '<em>' . t('* Save settings before testing the connection.') . '</em><div id="gwo-test-connection-results"></div>'
    );
  }

  return system_settings_form($form);
}

/**
 * GWO admin settings form validation function.
 */
function gwo_settings_admin_form_validate($form, $form_state) {
  $v = $form_state['values'];
  $private_key_path = DRUPAL_ROOT . '/' . $v['gwo_service_account_private_key'];
  if (!file_get_contents($private_key_path)) {
    form_set_error('gwo_service_account_private_key', t('Could not find the private key file located at: %path', array('%path' => $private_key_path)));
    return;
  }
}

/**
 * Submit callback to test the connection with Google.
 */
function gwo_settings_admin_form_test_connection($form, $form_state) {
  $date = date('m/d/y H:i:s', REQUEST_TIME);
  try {
    $service = gwo_service();
    // Try to get some data, even if there is none, will let us know
    // if the connection is successful.
    $result = $service->loyaltyclass->listLoyaltyclass(_gwo_issuer_id(), array('maxResults' => 1));
  } catch (Exception $e) {
    return '<span class="error">' . $date . ': ' . $e->getMessage() . '</span>';
  }
  return t('@time: API Connection was successful.', array('@time' => $date));
}

